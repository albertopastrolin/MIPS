#Pastrolin Alberto

.data
  vector: .word 1,2,3,4,5				#the vector where we wont to find the key
  n: .word 5							#the length of the vector
  key: .word 2						#the number that we wont to find
	
.text
  lw $a1, key						#prepare the key
  move $a2, $zero					#prepare the left index
  lw $a3, n
  subi $a3, $a3, 1					#prepare the right index
  jal __binary						#research
  j __end							#print the result
	
  __binary:
    subi $sp, $sp, 4				#create space for the return adress
    sw $ra, 0($sp)					#save the return adress
    bge $a2, $a3, __not_present		#if the left index is grater or equal of the right index the key isn't present into the vector
    add $t0, $a2, $a3				#sum the two index, because to do the research into the center we need the mid value
    add $t2, $t0, $t0				#but because the adress is a multiple of 4, we first multiply the result of the sum for 2
    div $t0, $t0, 2					#and then we calculate the mid value (it is only becuase it is nosense to first divide and then multiply the same number)
    lw $t1, vector($t2)				#load the mid value
    beq $a1, $t1, __found			#if the mid value is equal to the key we have finished
    blt $a1, $t1, __less				#if the key is smaller or greater than the mid value, we need to modify one index
    move $a2, $t0					#in the case of a greater key, we modify the left index
    jal __binary					#recursive call
    j __jump
    __less:
      move $a3, $t0				#in the case of a smaller key, we modify the right index
      jal __binary				#recursive call
      j __jump
    __found:
      move $v0, $t0				#set the result as the position of the key
      j __jump
    __not_present:
      li $v0, -1					#if the key isn't present, we set the result to -1
    __jump:
      lw $ra, 0($sp)				#load the last adress from the stack
      addi $sp, $sp, 4			#update the stack
      jr $ra					#return back
	
  __end:
    move $a0, $v0
    li $v0, 1
    syscall						#print the result
    li $v0, 10
    syscall
