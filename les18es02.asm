#Pastrolin Alberto
#31/12/2012

.data
  var_n: .word 6
.text
  move $v0, $zero			#setting to 0 the result
  move $a0, $zero			#i
  lw $a1, var_n				#j
  jal __sum					#function call
  j __end					#print the result
	
  __sum:
    beq $a0, $a1, __equal	#i=j
		
    subi $sp, $sp, 12		#preparing the stack for saving
    sw $ra, 8($sp)			#return adress stored
    sw $a1, 4($sp)			#j stored
    sw $a0, 0($sp)			#i stored
    add $t0, $a0, $a1
    div $a1, $t0, 2
    jal __sum				#[i, (i+j)/2]
		
    lw $ra, 8($sp)			#catch the return adress
    lw $a1, 4($sp)			#catch j
    lw $a0, 0($sp)			#catch i
    addi $sp, $sp, 12		#resizing the stack
    add $t0, $a0, $a1
    div $t0, $t0, 2
    addi $a0, $t0, 1
    j __sum				#{[(i+j)/2]+1, j}
  __equal:
    add $v0, $v0, $a0		#adding the value to the result
    jr $ra
	
  __end:
    move $a0, $v0
    li $v0, 1
    syscall				#print the result
    li $v0, 10
    syscall
