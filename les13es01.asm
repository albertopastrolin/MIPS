#Pastrolin Alberto
#28/11/2012

.text

__read_n:
  li $v0, 5			#read int mode
  syscall				#read n
  move $t0, $v0			#move n into a temporary register

__loop:
  __wait:
    lw $t1, 0xFFFF0000	#control if there is a new char
    beqz $t1, __wait	#if not, continue to wait
		
  __read:
    lw $t2, 0xFFFF0004	#read the new char
		
  __manipulate:
    blt $t2, 58 __iterate	#delete the decimal char
    blt $t2, 91 __print	#don't modify the upper char
    subi $t2, $t2, 32	#upper case of the char
		
  __print:
    sw $t2, 0xFFFF000C	#print the result of the manipulation
		
  __iterate:
    subi $t0, $t0, 1
    bnez $t0, __loop	#loop
		
  li $v0, 10
  syscall