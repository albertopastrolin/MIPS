#Pastrolin Alberto
#05/12/2012
.data
  n: 11
  palindrome: .asciiz "\nLa stringa è palindroma"
  not_palindrome: .asciiz "\nLa stringa non è palindroma"
  s: .asciiz "0123443210"
	
.text
  li $v0, 8				#read string mode
  la $a0, s				#adress of the string buffer
  lw $a1, n				#string length (BUT WE CAN READ ONLY N-1 ELEMENT!!!)
  syscall
  move $t0, $zero			#left index, it starts at the first element of the string
  lw $t1, n
  subi $t1, $t1, 2			#right index, it starts at the last element of the string
  #THE RIGHT INDEX IS POINTED TO N-2 BECAUSE THE STRING MAX LENGTH IS N-1 AND THE LAST CHAR IS \0
  la $a0, palindrome			#i'm optimist, so i presume that the string is palindrome
	
  __palindrome:
    lb $t2, s($t0)			#read the first unread element from the left part of the string
    lb $t3, s($t1)			#read the first unread element from the right part of the string
    bne $t2, $t3, __not		#the elements are different so the string isn't palindrome :(
    addi $t0, $t0, 1		#next element from the left
    subi $t1, $t1, 1		#next element from the right
    blt $t0, $t1, __palindrome	#if the left index has surpassed the right index we have finished to check
		
  j __print				#and the string is palindrome :)
	
  __not:
    la $a0, not_palindrome		#read the message for the user
	
  __print:
    li $v0, 4			#print an advice for the user
    syscall
	
  li $v0, 10
  syscall
