#Pastrolin Alberto
#21/11/2012

.data

n: 5					#the number of elements that we wont to read

.text

li $t0, 0				#the sum of the elements that we have already read
li $t1, 0				#the number of elements that we have already read
lw $t2, n				#load the number of elements that we need to read

__load:
  li $v0, 5			#read integer mode
  syscall				#read the element
  add $t0, $t0, $v0		#sum the new element to the old elements
  addi $t1, $t1, 1		#we have read another element
  beq $t1, $t2, __result		#if we have read all the element than we calculate the result
  j __load			#return up
	
__result:
  li $v0, 1			#print integer mode
  div $a0, $t0, $t1		#quickly we put the result into $a0
  syscall				#print the result

  li $v0, 10
  syscall