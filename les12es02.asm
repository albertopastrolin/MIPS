#Pastrolin Alberto
.text
  li $v0, 5
  syscall
  move $s0, $v0					#n
  li $v0, 5
  syscall
  move $s1, $v0					#k

  __loop:
    __fetch:
      lb $t0, 0xFFFF0000		#RC
      beqz $t0, __fetch			#Loop until a char isn't digited
    lb $t1, 0xFFFF0004			#RD
    add $t1, $t1, $s1			#shifting the number
    blt $t1, 126, __print			#have we done an ASCII's overflow?
    subi $t1, $t1, 95			#if yes, we subtract 95 to return at the begin of the standard printable ASCII
    __print:
    sb $t1, 0xFFFF000C			#and then we print the crypthed char
    subi $s0, $s0, 1
    bnez $s0, __loop
		
  li $v0, 10
  syscall
