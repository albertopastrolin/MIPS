#Pastrolin Alberto
#modification of the exercise 2 lesson 12 for the exam
.text
  li $v0, 5
  syscall
  move $s0, $v0					#n
  li $v0, 5
  syscall
  move $s1, $v0					#k

  __loop:
    __fetch:
      lb $t0, 0xFFFF0000		#RC
      beqz $t0, __fetch			#Loop until a char isn't digited
    lb $t1, 0xFFFF0004			#RD
    sub $t1, $t1, $s1			#shifting the number
    bge $t1, 90, __minuscole		#maiuscole/minuscole?
    bge $t1, 65, __print			#no need to realign the number
    add $t1, $t1, 26			#if we have an upper char and it isn't ok, we realign it
    j __print
    __minuscole:
    bge $t1, 97, __print			#no need to realign the number
    add $t1, $t1, 26			#if we have a lower char and it isn't ok, we realign it
    __print:
    sb $t1, 0xFFFF000C			#and then we print the crypthed char
    subi $s0, $s0, 1
    bnez $s0, __loop
		
  li $v0, 10
  syscall