#Pastrolin Alberto
#05/12/2012
.data
  k: 7				#declaration of the number that we wont to find
  n: 10				#number of elements
  v: .byte 0:10			#first declaration of the vector
	
.text
  move $t0, $zero			#counter
  lw $t1, n			#n
  lw $t2, k			#k
  move $t3, $zero			#vector's index
	
  __read:				#read the elements of the vector
    li $v0, 5
    syscall
    sb $v0, v($t3)		#store an element into the vector
    addi $t3, $t3, 4
    addi $t0, $t0, 1
    bne $t0, $t1, __read
	
  move $t0, $zero			#counter
  move $t3, $zero			#vector's index
  li $t5, -1			#index of k
	
  __search:			#search k into the vector
    lb $t4, v($t3)
    beq $t4, $t2, __set
    j __inc
			
    __set:			#find k
      move $t5, $t0
      j __print
		
    __inc:			#go go go go
      addi $t0, $t0, 1
      addi $t3, $t3, 4
      bne $t0, $t1, __search
			
  __print:			#print the result
    move $a0, $t5
    li $v0, 1
    syscall
	
  li $v0, 10
  syscall
