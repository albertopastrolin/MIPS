#Pastrolin Alberto
#11/12/2012

.data
vector: .word 1, 2, 3, 4, 5			#declaretion of the vector
max: .word 16					#vector length

.text
  move $a0, $zero				#first index to the begin of the vector
  lw $a1, max				#second index to the end of the vector
  jal __reverse
	
  move $t0, $zero				#index
  lw $a1, max				#end of the vector
  li $v0, 1				#print integer mode
  jal __print
	
  __reverse:
    lw $t0, vector($a0)		#load an element from the begin of the vector
    lw $t1, vector($a1)		#load an element from the end of the vector
    sw $t0, vector($a1)		#store the first element into the position of the second element
    sw $t1, vector($a0)		#store the second element into the position of the first element
    addi $a0, $a0, 4		#increment the first index
    subi $a1, $a1, 4		#decrement the second index
    ble $a0, $a1, __reverse		#if the first index is lesser than the second index we haven't finished
    jr $ra
	
  __print:
    lw $a0, vector($t0)		#load a number from the vector
    syscall				#print a number
    addi $t0, $t0, 4		#next value
    ble $t0, $a1, __print
	
  li $v0, 10
  syscall
