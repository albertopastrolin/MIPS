#Pastrolin Alberto
#19/12/2012

.data
n: .word 5
.text
  move $v0, $zero				#initializzation of the final value
  lw $t0, n
  subi $sp, $sp, 4			
  sw $t0, 0($sp)				#charging n into the stack
  jal __sum				#function call
  move $a0, $v0				#loading the result of __sum into the argument of __print
  j __print				#and now, to __print!!!

  __sum:					#return the sum of the first n natural number (too much n...)
    lw $t0, 0($sp)			#we keep n from the stack
    beqz $t0, __go_back		#n is equal to 0?
    subi $sp, $sp, 8		#no, so we need to redo the function :-)
    sw $ra, 4($sp)			#we save the return adress
    add $v0, $v0, $t0		#calculating the final value...
    subi $t0, $t0, 1		#the next argument of __sum
    sw $t0, 0($sp)			#now stored into the stack
    jal __sum			#recursive call
    addi $sp, $sp, 8		
    lw $ra, 4($sp)			#retrying the return adress from the stack
    __go_back:
      jr $ra			#and now we came back
	
	
  __print:
    li $v0, 1			#print-int mode setted correctly :-)
    syscall				#printing out the result
    li $v0, 10
    syscall
