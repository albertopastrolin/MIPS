#Pastrolin Alberto
#28/11/2012
.text
  li $t1, 4				#number of char to read
  li $t4, 0				#result
	
  __loop:
    lw $t0, 0xFFFF0000
    beqz $t0, __loop		#wait for a new char
    lw $t0, 0xFFFF0004		#read the char
    subi $t2, $t1, 1		#the positional value of the char
    subi $t3, $t0, 0x30		#char to integer
		
    __pow:
      beqz $t2, __iterate	#end of the value incrementation
      add $t3, $t3, $t3	#n*2
      subi $t2, $t2, 1
      j __pow
		
    __iterate:
      add $t4, $t4, $t3	#update the result
      subi $t1, $t1, 1	#update the counter
      bnez $t1, __loop
	
  blt $t4, 0x9, __dec			#the result is lesser than 10
  j __hex					#the result is greater
	
  __dec:
    addi $t4, $t4, 	0x30		#we transform the number from an integer to a char (0->9)
    j __print
	
  __hex:
    addi $t4, $t4, 55		#we transform the number from an integer to a char (A->F)
	
  __print:
    sw $t4, 0xFFFF000C		#print
		
  li $v0, 10
  syscall