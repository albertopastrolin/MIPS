#Pastrolin Alberto
#21/11/2012
.text
  li $v0, 5			#read integer mode
  syscall				#syscall to read m
  move $t0, $v0			#we store m in  $t0

  li $v0, 5			#read integer mode
  syscall				#syscall to read n
  move $t1, $v0			#we store n in $t1
	
  ble $t1, $t0, __loop  		#if $t1 is less or equal than $t0 we do the difference
  j __error			#else we print an error

__loop:
  beq $t0, $t1 __print  		#$t0 isn't equal to $t1, so we have finished to do the difference
  addi $t1, $t1, 1	
  addi $t2, $t2, 1		#$t2 is our difference
  j __loop			#we need to do another increment

__print:
  li $v0, 1			#print integer mode
  move $a0, $t2			#we move the result of the difference in the $a0 register to print it
  syscall				#print the result
  j __end
	
__error:
  li $v0, 1
  li $a0, -1			#we print -1 to segnalate the error
  syscall
	
__end:
  li $v0, 10
  syscall
